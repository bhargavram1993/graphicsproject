var projection; // NEW! global variable to hold the projection matrix

var left = -2.0;
var right = 2.0;
var ytop = 5.0;
var bottom = -4.0;
var near = -2;
var far = 2;
// NEW! Set up a simple oblique, orthographic projection matrix
projection = ortho( left, right, bottom, ytop, near, far );
projection = mult(projection, rotate(-70, vec3(1,0,0)));
projection = mult(projection, rotate(25, vec3(0,0,1)));

var radius = 5.0;
var theta  = 0.0;
var phi    = 0.0;
var  fieldofview = 50.0; //Field-of-view in Y direction angle (in degrees)
var  aspect = 1.0;

var eye;
var at = vec3(0.0, 0.0, 0.0);
//var up = vec3(0.0, 1.0, 0.0);
var up = vec3(0.0, 0.0, 1.0);

var angle = [0,0,0];
var angleProj;
var canWidth;
var canHeight;



/* Initialize global WebGL stuff - not object specific */
function initGL(){
    // local variable to hold a reference to an HTML5 canvass
    var canvas = document.getElementById( "gl-canvas" );
	canWidth = canvas.width;
	canHeight = canvas.height;
	
    // obtain a WebGL context bound to our canvas
    var gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height ); // use the whole canvas
    gl.clearColor( 0.0, 0.0, 0.0, 1.0 ); // background color
	
	gl.enable(gl.DEPTH_TEST); // NEW! make sure the GPU draw back to front

    return gl; // send this back so that other parts of the program can use it
}

/* Load shaders and initialize attribute pointers. */
function loadShaderProgram(gl){
    // use the existing program if given, otherwise use our own defaults
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    // get the position attribute and save it to our program object
    //   then enable the vertex attribute array
    program.vposLoc = gl.getAttribLocation( program, "vPosition" );
    gl.enableVertexAttribArray( program.vposLoc );
    // get the address of the uniform variable and save it to our program object
    program.colorLoc = gl.getUniformLocation( program, "color" );
	program.colorLocTwo = gl.getUniformLocation( program, "colorTwo" );
	//get the normal attribute and save it to our program object
	program.vNormalLoc = gl.getAttribLocation(program, "vNormal" );
	
	
	// NEW! get the address of the uniform variable and save it to our program object
	program.projLoc = gl.getUniformLocation( program, "proj" );
	program.MultMat = gl.getUniformLocation(program, "Mat");
	angleProj = gl.getUniformLocation(program, "angle");
	
	program.heightmin = gl.getUniformLocation( program, "hmin" );
	program.heightmax = gl.getUniformLocation( program, "hmax" );
	
	
	
    return program; // send this back so that other parts of the program can use it
	
}

/* Global render callback to draw all objects */
function renderToContext(drawables, gl){
    // inner-scoped function for closure trickery
    function renderScene(){
        renderToContext(drawables, gl);
    }

    // start from a clean frame buffer for this frame
    gl.clear( gl.COLOR_BUFFER_BIT);

    drawables.forEach(function(obj){ // loop over all objects and draw each
        obj.draw(gl);
    });

    // queue up this same callback for the next frame
    requestAnimFrame(renderScene);
}

/* Constructor for a triangle strip object (initializes the data). */
function TriStrip(gl, program, color,colorTwo){
    this.program = program; // save my shader program
    this.color = color; // the color of this triangle strip surface
	this.colorTwo=colorTwo;
    this.data = mkstrip(); // this array will hold raw vertex positions
    this.vBufferId = gl.createBuffer(); // reserve a buffer object and store a reference to it
	this.vNormalsId=gl.createBuffer();

    gl.bindBuffer( gl.ARRAY_BUFFER, this.vBufferId ); // set active array buffer
    // pass data to the graphics hardware (convert JS Array to a typed array)
    gl.bufferData( gl.ARRAY_BUFFER, flatten(this.data.points), gl.STATIC_DRAW );
	
	 gl.bindBuffer( gl.ARRAY_BUFFER, this.vNormalsId ); // set active array buffer
    // pass data to the graphics hardware (convert JS Array to a typed array)
    gl.bufferData( gl.ARRAY_BUFFER, flatten(this.data.normals), gl.STATIC_DRAW );
	
	
	//array element buffer
	var elementBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, elementBuffer);
	console.log(this.data.vertices.length);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.data.vertices), gl.STATIC_DRAW);

}

/* Method allows an object to render itself */
TriStrip.prototype.draw = function(gl){
    gl.useProgram( this.program ); // set the current shader programs

    gl.bindBuffer( gl.ARRAY_BUFFER, this.vBufferId ); // set pos buffer active
    // map position buffer data to the corresponding vertex shader attribute
    gl.vertexAttribPointer( this.program.vposLoc, 3, gl.FLOAT, false, 0, 0 );

    // send this object's color down to the GPU as a uniform variable
    gl.uniform4fv(this.program.colorLoc, flatten(this.color));
   gl.uniform4fv(this.program.colorLocTwo, flatten(this.colorTwo));
   
//	gl.vertexAttribPointer( this.data.vNormal, 4, gl.FLOAT, false, 0, 0 );
	//gl.enableVertexAttribArray( this.program.vNormalLoc);

   
   gl.uniform1f(this.program.heightmin, DEMObj.hmin);
   gl.uniform1f(this.program.heightmax, DEMObj.hmax);
   
   // NEW! send the global projection matrix to the shader program
	gl.uniformMatrix4fv(this.program.projLoc, gl.FALSE, flatten(projection));
	gl.uniformMatrix4fv(this.program.MultMat , gl.FALSE , flatten(MultiplyMat));
    
    // render the primitives!
    //gl.drawArrays(gl.TRIANGLE_STRIP, 0, this.vertices.length);
	gl.drawElements(gl.TRIANGLE_STRIP, this.data.vertices.length, gl.UNSIGNED_SHORT, 0 );
}
var MultiplyMat;

/* Build a triangle strip with random heights. */
function mkstrip(){

	var h, i,j = 0; // best practice in JS is to declare our variables up front
    var points = [[]]; // to hold the individual coordinate triples
    var vertices = []; // to hold the vertices to be drawn as tri-strips
	
	var normalsArray=[[]];
	var normalUL;
	var normalUR;
	var normalLL;
	var normalLR;

    // generate a thin 10x10 grid of points with random heights
	for( j= 0; j< DEMObj.ncols ; j++)
	{
		points[j]=new Array(DEMObj.nrows);
      for (i = 0; i < DEMObj.nrows; i++) {
				points[j][i]= vec3((DEMObj.xmin + (i * DEMObj.xres)), (DEMObj.ymin+ (j * DEMObj.yres)), (DEMObj.heights[j][i]) - DEMObj.hmin);

				}
		
	}
	
	var translateMat = translate(- ((DEMObj.xmax + DEMObj.xmin) / 2), - ((DEMObj.ymax + DEMObj.ymin)/2), - ((DEMObj.hmax + DEMObj.hmin)/2) );
	
	var scaleMat = mat4(
	vec4(scale((2/(DEMObj.xmax - DEMObj.xmin)),[1,0,0,0])),
	vec4(scale((2/(DEMObj.ymax - DEMObj.ymin)),[0,1,0,0])),
	vec4(scale((2/(DEMObj.hmax - DEMObj.hmin)),[0,0,1,0])),
	vec4([0,0,0,1])
	);
	
	MultiplyMat = mult(scaleMat , translateMat);
	
	
	//logic for grid with degeneric triangles.
	var count = 0;
	for(i=0; i < (DEMObj.nrows * DEMObj.ncols) - DEMObj.nrows; i++){
		vertices.push(i,[i + DEMObj.nrows]);
	
		if(count === DEMObj.nrows){
			vertices.push(i+DEMObj.nrows,[i+1]);
			count = 0;
		}
	}
	
	//calculate normals for points
	
	for(j=0;j<DEMObj.ncols; j++)
	{
		for(i=0;i<DEMObj.nrows;i++)
		{
			normalsArray[j]=new Array(i);
			//initialize normals to zero
			normalUL=[0,0,0,0]; normalUR=[0,0,0,0];
			normalLL=[0,0,0,0];normalLR=[0,0,0,0];
			
			//Lower left triangle normal
			 if(j<DEMObj.ncols-1 && i>0)
			{
				var n1=subtract(points[j][i-1],points[j][i]);
				var n2=subtract(points[j+1][i],points[j][i]);
				 
				normalLL = normalize(cross(n2, n1));
				normalLL = vec4(normalLL);
			}	
			//Lower right triangle normal
			 if(j>0 && i>0)
			{
				var n1=subtract(points[j-1][i],points[j][i]);
				var n2=subtract(points[j][i-1],points[j][i]);
				 
				normalLR = normalize(cross(n2, n1));
				normalLR = vec4(normalLR);
			}	
			
			//Upper right triangle normal calculation
			if(j<DEMObj.ncols-1 && i<DEMObj.nrows-1)
			{
				var n1=subtract(points[j][i+1],points[j][i]);
				var n2=subtract(points[j+1][i],points[j][i]);
				 
				normalUR = normalize(cross(n2, n1));
				normalUR = vec4(normalUR);
				
			}
			//Upper left triangle normal calculation
			 if(j>0 && i<DEMObj.nrows-1)
			{
				var n1=subtract(points[j][i+1],points[j][i]);
				var n2=subtract(points[j-1][i],points[j][i]);
				 
				normalUL = normalize(cross(n2, n1));
				normalUL = vec4(normalUL);
			}	
			
			var finalNormal=normalize(add(add(normalUR,normalUL),add(normalLR,normalLL)));
			normalsArray[j][i]=vec4(finalNormal);
		}
	}

	
    return {points: points,vertices: vertices ,normals:normalsArray};
}

/* Set up event callback to start the application */
function demacess(){
    
    // local variable to hold reference to our WebGL context
    var gl = initGL(); // basic WebGL setup for the scene
    var prog = loadShaderProgram(gl);
	
    document.getElementById("nameoffile").innerHTML=DEMObj["cellname"];

    // event listener on the button will set the color of each drawable object
    document.getElementById("colorBtn").addEventListener("click",function()
	{
       var  color = vec4( document.getElementById("redOne").value,
                          document.getElementById("greenOne").value,
                          document.getElementById("blueOne").value,
                          1.0);
						  
						  
        drawables.forEach(function(obj)
		{
            obj.color = color;
			
        });
  
		
    });
	document.getElementById("colorBtn1").addEventListener("click",function()
	{
	var colorTwo=vec4(document.getElementById("redTwo").value,
		                  document.getElementById("greenTwo").value,
						  document.getElementById("blueTwo").value,1);
						  
        drawables.forEach(function(obj)
		{
            
			obj.colorTwo=colorTwo;
        });						  
	});
	
	document.getElementById("slider1").addEventListener("change",function()
	{
		angle[0] = document.getElementById("slider1").value;
		gl.uniform3fv(angleProj, angle);
	});
    document.getElementById("slider2").addEventListener("change",function()
	{
		angle[1] = document.getElementById("slider2").value;
		gl.uniform3fv(angleProj, angle);
	});
	document.getElementById("slider3").addEventListener("change",function()
	{
		angle[2] = document.getElementById("slider3").value;
		gl.uniform3fv(angleProj, angle);
	});
	
	
	
	
    var drawables = []; // used to store a list of objects that need to be drawn

    // create a triangle strip object and add it to the list
    drawables.push( new TriStrip(gl, prog, vec4(1,0,0,1),vec4(0,0,1,1)) );

    renderToContext(drawables, gl); // start drawing the scene
}

function demcall()
	{
	var demfile = document.getElementById("dembrowse").files[0];
	readDemFile(demfile, demacess);		
	}
	
	
function projectiontype()
{
	var projtype = document.getElementById("projectionbtn").value;
	if(projtype == "perspective")
	{
		eye = vec3(radius * Math.sin(theta)*Math.cos(phi), 
				radius * Math.sin(theta)*Math.sin(phi), 
				radius * Math.cos(theta));
	
		modelViewMatrix = lookAt(eye, at , up);
		projection = mult(perspective(fieldofview, aspect, 0.1, 10),modelViewMatrix );
	}
	else if(projtype == "parallel")
	{
		projection = ortho(left, right,bottom, ytop, near, far); 
		projection = mult(projection, rotate(-70, vec3(1,0,0)));
		projection = mult(projection, rotate(30, vec3(0,0,1)));
	}
}

function cameraview()
{
	var projType = document.getElementById("projectionbtn").value;
	if(projType == "perspective")
	{
		//camera change 
		at = vec3(0.0,document.getElementById("slider5").value,0.0);
		modelViewMatrix = lookAt(eye, at , up);
		projection = mult(perspective(fieldofview, aspect, 0.1, 10),modelViewMatrix );
	}
	else
	{
		projection = ortho(parseInt(left), parseInt(right),parseInt(bottom) - parseInt(document.getElementById("slider5").value),
							parseInt(ytop) + parseInt(document.getElementById("slider5").value), parseInt(near), parseInt(far)); 
		projection = mult(projection, rotate(-70, vec3(1,0,0)));
		projection = mult(projection, rotate(30, vec3(0,0,1)));
	}
}

function zoom()
{
	var projType = document.getElementById("projectionbtn").value;
	if(projType == "perspective")
	{
		// change fovy from perspective
		projection = mult(perspective(document.getElementById("slider4").value, aspect, 0.1, 100),modelViewMatrix );
		
	}else
	{
		var change = document.getElementById("slider4").value;
		//change ortho params for othographic
		projection = ortho(left - parseInt(change),
								right +  parseInt(change),
								bottom -  parseInt(change),
								ytop + parseInt(change),
								near - parseInt(change),
								far + parseInt(change)); 
		projection = mult(projection, rotate(-70, vec3(1,0,0)));
		projection = mult(projection, rotate(30, vec3(0,0,1)));
	}
	
}
